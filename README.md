**LIS4369 Fall 2015 A2**

**Loucil Rubio**

## Simple Calculator C# Program

Valid Operation:
![Simple Calculator Screenshot 1](https://bitbucket.org/loucilvr/lis4369_fall15_a2/raw/master/images/hw2-1.png "Valid Operation")

Invalid Operation:
![Simple Calculator Screenshot 2](https://bitbucket.org/loucilvr/lis4369_fall15_a2/raw/master/images/hw2-2.png "Invalid Operation")

Extra Credit:
![Simple Calculator Extra Credit Screenshot](https://bitbucket.org/loucilvr/lis4369_fall15_a2/raw/master/images/hw2-ec.png "Extra Credit")
